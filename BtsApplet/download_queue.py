# -*- coding: utf-8 -*-

"""
  debian-bts-applet - GNOME applet for monitoring Debian bugs
  Copyright (C) 2008  Chris Lamb <chris@chris-lamb.co.uk>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import threading

class DownloadQueue(object):
    def __init__(self, fn):
        self.fn = fn
        self._sem = threading.Semaphore(0)
        self._lock = threading.Lock()
        self._queue = []
        self._threads = []
        self._running = True

    def enqueue(self, iter, bug_number):
        self._lock.acquire()
        self._queue.append((iter, bug_number))
        self._lock.release()
        self._sem.release()

    def run(self, workers):
        self._threads = [threading.Thread(target=self._loop) for num in range(workers)]
        for thread in self._threads:
            thread.start()

    def stop(self):
        self._running = False
        for thread in self._threads:
            self._sem.release()

    def is_active(self):
        return reduce(bool.__or__, [t.isAlive() for t in self._threads])

    def _loop(self):
        while self._running:
            self._sem.acquire()
            if not self._running: return
            self._lock.acquire()
            iter, bug_number = self._queue[0]
            self._queue = self._queue[1:]
            self._lock.release()
            try:
                self.fn(iter, bug_number)
            except:
                import traceback
                traceback.print_exc()
