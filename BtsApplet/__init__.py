# -*- coding: utf-8 -*-

"""
  debian-bts-applet - GNOME applet for monitoring Debian bugs
  Copyright (C) 2008  Chris Lamb <chris@chris-lamb.co.uk>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from model import Model
from view import View
from controller import Controller

from bug_number_parser import BugNumberParser
from download_queue import DownloadQueue

ST_NEW, ST_DOWNLOADING, ST_ERROR_DOWNLOADING, ST_UPDATING, ST_ERROR_UPDATING, ST_DOWNLOADED = range(6)

FIELDS = ['bug_number', 'state', 'package', 'summary', 'severity', 'tags', 'status', 'tick']
FIELD_TYPES = (int, int, str, str, str, str, str, int)

TICK_INTERVAL_SECONDS = 60
TICK_DEFAULT = 1
TICK_ERROR_RESET = 3

# Number of worker threads for downloading bugs
NUM_WORKERS = 3

def open_url(url):
    import os
    [fd.close() for fd in os.popen2('/usr/bin/gnome-open %s' % url)]
