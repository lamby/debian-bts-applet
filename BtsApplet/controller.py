# -*- coding: utf-8 -*-

"""
  debian-bts-applet - GNOME applet for monitoring Debian bugs
  Copyright (C) 2008  Chris Lamb <chris@chris-lamb.co.uk>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import gtk
import gobject
import os
import sys
import re
import time

import BtsApplet

class Controller(object):
    def __init__(self, applet, data_dir):
        self.applet = applet

        if applet:
            gconf_base = applet.get_preferences_key()
            applet.connect("destroy", self.on_applet_destroy)
        else:
            gconf_base = '/apps/bts-applet'

        self.model = BtsApplet.Model(self, gconf_base)
        self.view = BtsApplet.View(self, applet, data_dir)
        self.view.xml.signal_autoconnect(self)
        self.view.setup_treeview(self.model.get_treemodel())

        self.running = True
        gobject.timeout_add(BtsApplet.TICK_INTERVAL_SECONDS * 1000, self.on_update_timer_cb)

    # Actions

    def do_standalone_init(self):
        self.view.do_applet_show()

    def do_notify(self, old, new, delta):
        self.view.do_notify(old, new, delta)

    def do_stop(self):
        self.running = False
        self.model.close()
        self.view.do_applet_hide()

        # We cannot call continue until all the worker threads have died.
        while True:
            if not self.model.is_active():
                break
            time.sleep(0.1)

        if not self.applet:
            gobject.timeout_add(0, gtk.main_quit)

    # Events

    def on_update_timer_cb(self):
        self.model.tick()
        return self.running

    def on_window_main_delete_event(self, *_):
        self.do_stop()

    def on_applet_destroy(self, *_):
        os.environ['GNOME_DISABLE_CRASH_DIALOG'] = '1'
        self.do_stop()

    def on_applet_click(self, _, event):
        if event.button == 1:
            self.view.do_applet_toggle()

    def on_btn_add_clicked(self, *_):
        if self._add_bug():
            self.view.reset_bug_entry()

    def on_entry_bug_number_activate(self, *_):
        if self._add_bug():
            self.view.reset_bug_entry(empty=True)

    def on_entry_bug_number_focus_in_event(self, *_):
        self.view.bug_number_focus_in()

    def on_entry_bug_number_focus_out_event(self, *_):
        self.view.bug_number_focus_out()

    def on_treeview_bugs_button_press_event(self, treeview, event):
        if event.button == 1 and event.type == gtk.gdk._2BUTTON_PRESS:
            iter = self.view.get_selected_bug_iter()
            bug_number = self.model.get_bug_number(iter)
            self.view.do_open_webbrowser(bug_number)
        if event.button == 3:
            self.view.do_popup_menu_bugs(treeview, event)

    def on_menuitem_remove_activate(self, *_):
        iter = self.view.get_selected_bug_iter()
        self.model.delete_bug(iter)

    def on_menuitem_info_activate(self, *_):
        iter = self.view.get_selected_bug_iter()
        bug_number = self.model.get_bug_number(iter)
        self.view.do_open_webbrowser(bug_number)

    def on_menuitem_refresh_activate(self, *_):
        iter = self.view.get_selected_bug_iter()
        self.model.refresh_bug(iter)

    def on_menuitem_show_bugs_activate(self, *_):
        self.view.do_applet_show()

    def on_menuitem_report_bug_activate(self, *_):
        cmd = None

        # Try reportbug-ng
        if os.path.isfile('/usr/bin/reportbug-ng'):
            cmd = '/usr/bin/reportbug-ng'
        # Try reportbug
        elif os.path.isfile('/usr/bin/reportbug'):
            cmd = '/usr/bin/x-terminal-emulator -e /usr/bin/reportbug'

        if cmd:
            [fd.close() for fd in os.popen2(cmd)]
        else:
            self.view.do_open_webbrowser(None)

    def on_menuitem_about_activate(self, *_):
        self.view.do_about_show()

    # Helpers

    def _add_bug(self):
        bug_string = self.view.get_bug_string()
        bug_number = BtsApplet.BugNumberParser().parse(bug_string)

        if not bug_number:
            return

        if self.model.contains_bug(bug_number):
            return

        return self.model.add_bug(bug_number)
