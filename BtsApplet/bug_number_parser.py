# -*- coding: utf-8 -*-

"""
  debian-bts-applet - GNOME applet for monitoring Debian bugs
  Copyright (C) 2008  Chris Lamb <chris@chris-lamb.co.uk>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import re

class BugNumberParser(object):
    PATTERNS = (
       re.compile(r'http(s)?://bugs.debian.(org|net)/cgi-bin/bugreport.cgi\?.*bug=(?P<bug>\d+)'),
       re.compile(r'http(s)?://bugs.debian.(org|net)/(?P<bug>\d+)'),
       re.compile(r'(?P<bug>\d+)(-\w+)?@(bugs.)?debian.org'),
       re.compile(r'#+(?P<bug>\d+)'),
    )

    def parse(self, txt):
        bug_number = self._parse(txt)

        if not bug_number:
            return False

        if not self._sanity_check(bug_number):
            return False

        return bug_number

    def _parse(self, txt):
        # See if we have a number
        try:
            bug_number = int(txt)
            return bug_number
        except ValueError:
            pass

        # Check against regexes
        for regex in self.PATTERNS:
            match = regex.search(txt)
            if not match:
                continue
            try:
                bug_number = int(match.group('bug'))
                return bug_number
            except ValueError:
                pass

        return False

    def _sanity_check(self, bug_number):
        # Check we have an integer object
        try:
            int(bug_number)
        except ValueError:
            return False

        # Check we are in a sensible range
        if not 100 < bug_number < 2000000:
            return False

        return True
