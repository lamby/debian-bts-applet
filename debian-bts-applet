#!/usr/bin/env python

"""
  debian-bts-applet - GNOME applet for monitoring Debian bugs
  Copyright (C) 2008  Chris Lamb <chris@chris-lamb.co.uk>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import os
import sys
import gtk
import gnomeapplet

import gettext
gettext.install('gnome-bts-applet', 'po/gen', unicode=True)

import BtsApplet

def find_data_dir():
    filename = 'debian-bts-applet.glade'
    for path in ['data/', '/usr/share/debian-bts-applet']:
        print >>sys.stderr, 'Checking %s' % path
        if os.path.isfile(os.path.join(path, filename)):
            return path

    print >>sys.stderr, 'Could not find data dir, exiting.'
    sys.exit(1)

controller = None
def factory(applet, iid):
    global controller
    print >>sys.stderr, "Constructing BtsApplet Controller"
    try:
        controller = BtsApplet.Controller(applet, find_data_dir())
    except:
        import traceback
        print >>sys.stderr, traceback.format_exc()
    gtk.gdk.threads_init()
    return True

def main():
    if len(sys.argv) == 2 and sys.argv[1] == '-w':
        factory(None, None)
        controller.do_standalone_init()

        try:
            gtk.init_check()
        except RuntimeError, e:
            sys.exit("E: %s. Exiting." % e)

        try:
            gtk.main()
            return 0
        except KeyboardInterrupt:
            controller.do_stop()
            return 1
    else:
        #sys.stderr = open('/tmp/debian-bts-applet.txt', 'a+', 0)
        print >>sys.stderr, "Starting Bonobo factory"
        gnomeapplet.bonobo_factory("OAFIID:DebianBTSApplet_Factory",
            gnomeapplet.Applet.__gtype__, 'DebianBTSApplet', '0', factory)

if __name__ == "__main__":
    sys.exit(main())
