#!/usr/bin/env python

import unittest
import sys
sys.path.append('..')

import gettext
gettext.install('gnome-bts-applet', 'po/gen', unicode=True)

import BtsApplet

class TestBugNumberParser(unittest.TestCase):
    def setUp(self):
        self.parse = BtsApplet.BugNumberParser().parse

    def testSimple(self):
        res = self.parse('123123')
        assert res == 123123
        assert type(res) is int

    def testEmpty(self):
        assert self.parse('') is False

    def testHigh(self):
        assert self.parse('99999999') is False

    def testLow(self):
        assert self.parse('50') is False

    def testNegative(self):
        assert self.parse('-1') is False

    def testInt(self):
        assert self.parse(123123) == 123123

    def testPrecedingHash(self):
        assert self.parse('#123123') == 123123

    def testPrecedingHashes(self):
        assert self.parse('#####123123') == 123123

    def testBDO(self):
        assert self.parse('http://bugs.debian.org/123123') == 123123

    def testBDOWithBookmark(self):
        assert self.parse('http://bugs.debian.org/123123#2') == 123123

    def testBDOSecure(self):
        assert self.parse('https://bugs.debian.org/123123') == 123123

    def testBDOWithNet(self):
        assert self.parse('http://bugs.debian.net/123123') == 123123

    def testBDOWithPreWhitespace(self):
        assert self.parse('  http://bugs.debian.org/123123') == 123123

    def testBDOWithPostWhitespace(self):
        assert self.parse('http://bugs.debian.org/123123  ') == 123123

    def testBDOWithPostRubbish(self):
        assert self.parse('http://bugs.debian.org/123123  rubbish') == 123123

    def testBDOTitle(self):
        assert self.parse('Debian Bug report logs - #123123') == 123123

    def testBDOFull(self):
        assert self.parse('http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=123123') \
            == 123123

    def testBDOFullWith(self):
        assert self.parse('http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=123123') \
            == 123123

    def testBDOMbox(self):
        assert self.parse('http://bugs.debian.org/cgi-bin/bugreport.cgi?mbox=yes;bug=123123') \
            == 123123

    def testEmail(self):
        assert self.parse('123123@bugs.debian.org') == 123123

    def testSubscribe(self):
        assert self.parse('123123-subscribe@bugs.debian.org') == 123123

    def testEmailNoBugs(self):
        assert self.parse('123123@debian.org') == 123123

    def testBTSControl(self):
        assert self.parse('Bug#123123: Summary message here') == 123123

    def testChangelog(self):
        assert self.parse('Bump Debhelper compatibility to 1337 (Closes: #123123)') == 123123


if __name__ == "__main__":
    unittest.main()
